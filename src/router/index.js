import Vue from 'vue'
import Router from 'vue-router'

let components_lists = () => import('../view/components_list.vue');
let view = () => import('../view/bar.vue');

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'components_lists',
      component: components_lists,
      children: [
        {
          path: '/*', component: view
        }
      ]
    }
  ]
})
