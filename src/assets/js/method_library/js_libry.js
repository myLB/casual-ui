class l_libry {
    constructor() {
        
    }
    l_blur_fixed(e,s) {
        let number=e.target.value.match(/(\d{1,2})?(\.\d{1,2})?/);//截取数字可有可无,如果有点后面必须有1-2位小数,也可以没点
        if(!number){
            s=e.target.value='';
        }else {
            let figure=Number(number[0]);//转化类型
            if(Number.isSafeInteger(figure)){//判断是否为一个安全整数，表示不超过某个范围
                s=figure.toFixed(2);
            }else {//进了这里说明是有小数的
                let reg=new RegExp("^\\d");
                if(!reg.test(number[0])) {//判断是否已数字开头的
                    return s = '0' + number[0];//已点开头的前面加个0
                }
                s=figure.toFixed(2);//防止只有一位小数
            }
        } 
    }//失焦时加上小数点
    limit(e,s) {
        let value=e.target.value.match(/\d+/);
        s=e.target.value = !value ? '' : value[0];
    }//只能输入数字
}

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
/*(new Date()).Format("yyyy-MM-dd")*/

export default l_libry