import CollapseTransition from './transitions/collapse-transition.js';

const components = [
  CollapseTransition
];
const install = function(Vue, opts = {}) {
  components.map(component => {
    Vue.component(component.name, component);
  });

  const ELEMENT = {};
  ELEMENT.size = opts.size || '';

  Vue.prototype.$ELEMENT = ELEMENT;
};

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}
module.export = {
  CollapseTransition,
};
module.export.default = module.export;
