import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    list: {
      aa: 1
    }
  },
  getters: {

  },
  mutations: {
    change(state, data) {
      state.list.aa = data;
    }
  },
  actions: {

  }
})
export default store
