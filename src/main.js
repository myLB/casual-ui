// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
/*import '@/assets/css/transition.less'*/
import "ag-grid/dist/styles/ag-grid.css";
import "ag-grid/dist/styles/ag-theme-balham.css";
const agGrid = require('ag-grid-enterprise/dist/ag-grid-enterprise.min');
agGrid.LicenseManager.setLicenseKey("Evaluation_License_Valid_Until_6_June_2018__MTUyODIzOTYwMDAwMA==4c69615c372b7cdc6c4bda8601ac106b");
import iView from 'iview';
import 'iview/dist/styles/iview.css';
Vue.use(ElementUI)
Vue.use(iView)
/*import ElCollapseTransition from '@/transitions/collapse-transition.js';
Vue.component(ElCollapseTransition.name, ElCollapseTransition);*/

Vue.config.productionTip = false
Vue.config.performance = true
Vue.directive('popover', {
  bind: function (el, binding, vnode) {
    const _ref = binding.expression ? binding.value : binding.arg;
    //console.log(el)
    //console.log(vnode.context)
    vnode.context.$refs[_ref].$refs.reference = el;
  },
  // 当被绑定的元素插入到 DOM 中时……
  /* inserted: function (el, binding, vnode) {
     // 聚焦元素
     console.log('inserted')
   },
   update: function (el, binding, vnode) {
     console.log('update')
   },
   componentUpdated: function (el, binding, vnode) {
     console.log('componentUpdated')
   },
   unbind: function (el, binding, vnode) {
     console.log('unbind')
   }*/
})
/*function LB () {

}
LB.options = {
  kk: 2
}
function initMixin (LB) {
  LB.mixin = function () {
    console.log(this)
  }
}
initMixin(LB)
LB.mixin()*/
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  customOption: 'foo',
  created(){
    //console.log(this.$options)
  }
})

