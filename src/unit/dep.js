//
class Dep{
  constructor(){
    //订阅的数组
    this.subs = []
  }
  //添加订阅
  addSub(watcher){
    this.subs.push(watcher);
  }
  notify(){
    //调用watcher的更新方法
    this.subs.forEach(watcher => watcher.update());
  }
}
